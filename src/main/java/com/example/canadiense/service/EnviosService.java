package com.example.canadiense.service;
import com.example.canadiense.service.DTO.EnviosDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface EnviosService {
    public ResponseEntity create(EnviosDTO enviosDTO);
    public Page<EnviosDTO> read(Integer pageSize, Integer pageNumber);
    public EnviosDTO update(EnviosDTO enviosDTO);
}
