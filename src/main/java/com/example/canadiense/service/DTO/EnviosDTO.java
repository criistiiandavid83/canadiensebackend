package com.example.canadiense.service.DTO;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class EnviosDTO  implements Serializable {
    @NotNull
    private int id;
    private String referencia;
    private String remitente;
    private  String descripcionContenido;
    private  String peso;
    private  String destinatario;
    private String direccion;
    private  String compañia;
    private  String ciudad;
    private  String pais;
    private  String codigoPostal;
    private LocalDate fecha;
}
