package com.example.canadiense.service.transformer;

import com.example.canadiense.domain.Envios;
import com.example.canadiense.service.DTO.EnviosDTO;

public class EnviosTransformer {
    public  static EnviosDTO getenviosDTOFromenvios(Envios envios){
        if (envios == null){
            return  null;
        }
        EnviosDTO dto = new EnviosDTO();

        //set variables
        dto.setId(envios.getId());
        dto.setReferencia(envios.getReferencia());
        dto.setRemitente(envios.getRemitente());
        dto.setDescripcionContenido(envios.getDescripcionContenido());
        dto.setPeso(envios.getPeso());
        dto.setDestinatario(envios.getDestinatario());
        dto.setDireccion(envios.getDireccion());
        dto.setCompañia(envios.getCompañia());
        dto.setCiudad(envios.getCiudad());
        dto.setPais(envios.getPais());
        dto.setCodigoPostal(envios.getCodigoPostal());
        dto.setFecha(envios.getFecha());

        return dto;
    }

    public  static Envios getenviosFromenviosDTO (EnviosDTO dto){
        if (dto == null){
            return null;
        }

        Envios envios = new Envios();
        envios.setId(dto.getId());
        envios.setReferencia(dto.getReferencia());
        envios.setRemitente(dto.getRemitente());
        envios.setDescripcionContenido(dto.getDescripcionContenido());
        envios.setPeso(dto.getPeso());
        envios.setDestinatario(dto.getDestinatario());
        envios.setDireccion(dto.getDireccion());
        envios.setCompañia(dto.getCompañia());
        envios.setCiudad(dto.getCiudad());
        envios.setPais(dto.getPais());
        envios.setCodigoPostal(dto.getCodigoPostal());
        envios.setFecha(dto.getFecha());



        return envios;
    }
}
