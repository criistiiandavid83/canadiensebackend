package com.example.canadiense.service;

import com.example.canadiense.domain.Envios;
import com.example.canadiense.repository.EnviosRepository;
import com.example.canadiense.service.DTO.EnviosDTO;
import com.example.canadiense.service.transformer.EnviosTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
@Service
public class EnviosServiceImp implements  EnviosService {
    @Autowired
    EnviosRepository enviosRepository;
    public ResponseEntity create(EnviosDTO enviosDTO) {
        Envios envios = EnviosTransformer.getenviosFromenviosDTO(enviosDTO);
        envios.setFecha(LocalDate.now());
        EnviosTransformer.getenviosDTOFromenvios(enviosRepository.save(envios));
        return new ResponseEntity(enviosDTO, HttpStatus.OK);
    }

    @Override
    public Page<EnviosDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return enviosRepository.findAll(pageable)
                .map(EnviosTransformer ::getenviosDTOFromenvios);
    }

    @Override
    public EnviosDTO update(EnviosDTO enviosDTO) {
        Envios envios = EnviosTransformer.getenviosFromenviosDTO(enviosDTO);
        EnviosTransformer.getenviosDTOFromenvios(enviosRepository.save(envios));
        return enviosDTO;

    }
}
