package com.example.canadiense.web.rest;
import com.example.canadiense.service.DTO.EnviosDTO;
import com.example.canadiense.service.EnviosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api")
@Api(value = "web service Envios", description = "web service for CRUD Envios")
public class EnviosResource {
    @Autowired
    EnviosService enviosService;

    @GetMapping("/envios")
    public Page<EnviosDTO> getAllByEnviosQuery(
            @PathParam("pageSize") Integer pageSize,
            @PathParam("pageNumber") Integer pageNumber){
        return  enviosService.read( pageSize,pageNumber);
    }
    @PostMapping("/envios")
    @ApiOperation(value = "Create envios", notes = "return a envios")
    public ResponseEntity create(@RequestBody EnviosDTO enviosDTO) {
        return enviosService.create(enviosDTO);
    }

    @PutMapping("/envios")
    public EnviosDTO update(@RequestBody EnviosDTO enviosDTO){

        return enviosService.update(enviosDTO);
    }
}



