package com.example.canadiense.repository;

import com.example.canadiense.domain.Envios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnviosRepository extends JpaRepository<Envios,Integer> {
}
