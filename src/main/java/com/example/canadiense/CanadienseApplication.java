package com.example.canadiense;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CanadienseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CanadienseApplication.class, args);
	}

}
