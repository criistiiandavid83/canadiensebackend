package com.example.canadiense.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Envios {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;
    private String referencia;
    private String remitente;
    private  String descripcionContenido;
    private  String peso;
    private  String destinatario;
    private String direccion;
    private  String compañia;
    private  String ciudad;
    private  String pais;
    private  String codigoPostal;
    private LocalDate fecha;
}
